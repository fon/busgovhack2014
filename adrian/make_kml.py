#!/usr/bin/env python
import psycopg2
from collections import OrderedDict

conn = psycopg2.connect("port=5433 dbname=govhack user=fon")
cur = conn.cursor()
cur.execute("select cardid, ondate, "
    "st_askml(ST_SetSRID(vector, 4326)) "
    "as line from lines;")


print '<?xml version="1.0" encoding="UTF-8"?>'
print '<kml xmlns="http://www.opengis.net/kml/2.2">'

for i,leg in enumerate(cur):
    (cardid, ts, line) = leg
    print """
  <Placemark>
    <name>[{}]   Cardid: {}</name>
    <description>Ondate: {}</description>
    {}
  </Placemark>
    """.format(i, cardid, ts, line)

print "</kml>"


"""
<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
  <Placemark>
    <name>Simple placemark</name>
    <description>Attached to the ground. Intelligently places itself 
       at the height of the underlying terrain.</description>
    <Point>
      <coordinates>-122.0822035425683,37.42228990140251,0</coordinates>
    </Point>
  </Placemark>
</kml>
"""    

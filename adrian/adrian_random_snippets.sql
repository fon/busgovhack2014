update smartdata set ontype=null where ontype='NULL';
update smartdata set onmode=null where onmode='NULL';
update smartdata set ondate=null where ondate='NULL';
update smartdata set onstation=null where onstation='NULL';
update smartdata set onlandmark=null where onlandmark='NULL';
update smartdata set offdate=null where offdate='NULL';
update smartdata set offstation=null where offstation='NULL';
update smartdata set offlandmark=null where offlandmark='NULL';

truncate smart;
insert into smart (cardid, ontype, onmode, ondate, onstation, onlandmark, offdate, offstation, offlandmark, distance)
select cast(cardid as int), cast(ontype as int), cast(onmode as int), cast(ondate as timestamp), onstation, cast(onlandmark as int),
       cast(offdate as timestamp), offstation, cast(offlandmark as int), cast(distance as float)
from smartdata
;



select s.cardid, s.ondate, s.onstation, t.location, st_y(t.geom) as lat, st_x(t.geom) as lon 
from smartdata s, stops t 
where s.onmode='1' and t.stoptype='Rail'
and lower(s.onstation) || ' stn'=lower(t.location)
limit 100;

\copy (select *, st_x(geom) as lon, st_y(geom) as lat from stops) to '/tmp/stops_full.csv' with csv header;
\copy sa1 to '/tmp/sa1_full.csv' with csv header;

select * from stops 
where stoptype='Rail'
limit 100;

index stops:
  stopnumber
  stoptype
  location
  gist(geom)

CREATE INDEX ON stops (stopnumber);
CREATE INDEX ON stops (stoptype);
CREATE INDEX ON stops (location);
CREATE INDEX ON stops USING GIST (geom);

clean sa1:
  delete from sa1 where ste_code11 <> '5'; -- remove non-WA
index sa1:
  gid (automatic pk)
  gist(geom)
CREATE INDEX ON sa1 USING GIST (geom);

clean sa2:
  delete from sa2 where ste_code11 <> '5'; -- remove non-WA
CREATE INDEX ON sa2 USING GIST (geom);

-- total traffic by stopnumber
create table stopcount as
with st as (
  select onlandmark as s from smart where onlandmark is not null
  union all
  select offlandmark as s from smart where offlandmark is not null
), 
st2 as (
  select s as stopnumber, count(*) as count
  from st
  group by s
)
select st2.stopnumber, st2.count, st_y(stops.geom) as lat, st_x(stops.geom) as lon
from st2, stops
where st2.stopnumber = stops.stopnumber
order by st2.stopnumber
;

-- hourly traffic by stopnumber
drop table if exists stopcount_hourly;
create table stopcount_hourly as
with st as (
  select onlandmark as s, ondate as ts1 from smart where onlandmark is not null
  union all
  select offlandmark as s, offdate as ts1 from smart where offlandmark is not null
), 
st2 as (
  select s as stopnumber, count(*) as count, date_trunc('h', ts1) as ts2
  from st
  group by s, ts2
)
select st2.stopnumber, st2.ts2 as ts, st2.count, st_y(stops.geom) as lat, st_x(stops.geom) as lon
from st2, stops
where st2.stopnumber = stops.stopnumber
order by st2.stopnumber, st2.ts2
;


-- patch up station table to fill railway stations
update station set station_landmark_id = (
select min(stopnumber) from stops where location = station_desc || ' Stn'
)
where station_landmark_id is null;

-- stops grouped by sa1
drop table if exists stops_sa1;
create table stops_sa1 as
select stops.gid as stops_gid, sa1.gid as sa1_gid -- could be stops.stopnumber, sa1.sa1_main11
from stops, sa1
where st_contains(sa1.geom, stops.geom);


-- centroids of sa2
drop table if exists sa2_centroid;
create table sa2_centroid as
select sa2.gid, st_centroid(sa2.geom) as centroid
from sa2;
CREATE INDEX ON sa2_centroid (gid);
CREATE INDEX ON sa2_centroid USING GIST (centroid);

-- stops grouped by sa2
drop table if exists stops_sa2;
create table stops_sa2 as
select stops.gid as stops_gid, sa2.gid as sa2_gid
from stops, sa2
where st_contains(sa2.geom, stops.geom);

CREATE INDEX ON stops_sa2 (stops_gid);
CREATE INDEX ON stops_sa2 (sa2_gid);

-- lines for each leg (could be journey), from sa2 -> sa2
drop table if exists lines;
create table lines as
with j as (
  select cardid, ondate, onlandmark, offlandmark 
  from smart where onmode=0 
  and onlandmark is not null and offlandmark is not null limit 100
)
select j.cardid, j.ondate, 
  st_makeline(on_c.centroid, off_c.centroid) as vector,
  st_distance_sphere(stops_on.geom, stops_off.geom) as distance
from j, stops stops_on, stops stops_off, stops_sa2 on_sa2, stops_sa2 off_sa2,
   sa2_centroid on_c, sa2_centroid off_c
where j.onlandmark = stops_on.stopnumber and j.offlandmark = stops_off.stopnumber
and stops_on.gid = on_sa2.stops_gid and on_sa2.sa2_gid = on_c.gid
and stops_off.gid = off_sa2.stops_gid and off_sa2.sa2_gid = off_c.gid
;


select cardid, ondate, st_askml(ST_SetSRID(vector, 4326)) as line from lines;



create table speed (start int, finish int, speed float, km_ratio float); -- start and finish are sa2.sa2_main11
insert into speed values (505031102, 505011072,12.2,16.89);  -- girraween to duncraig
insert into speed values (505011075, 505011078, 18.6,15.21); -- hillarys to kingsley





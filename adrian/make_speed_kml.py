#!/usr/bin/env python
import psycopg2

def convert_to_rgb(minval, maxval, val):
    # colors = [(245,10,10), (245,60,60), (245,130,130), (245, 230, 230)] # red - pink - white
    # colors = [(245, 230, 230), (245,130,130), (245,60,60), (245,10,10), ] # red - pink - white
    # colors = [(245, 230, 230), (245,130,130), (245,60,60), (245,10,10), ] # red - pink - white
    colors = [(10,10,245), (50,50,245), (150,150,150), (160,160,160), (170,170,170), (175,175,175), (180,180,180), (200,200,200), (230,230,230)]
    # colors = [(255, 0, 0), (128,128,0), (0, 255, 0), (0, 0, 255)]  # [RED, GREEN, BLUE] so low speed is RED!
    # colors = [(0, 0, 255), (0, 255, 0), (255, 0, 0)]  # [BLUE, GREEN, RED]
    # colors = [(128, 0, 0), (255, 0, 0)]  # [light red, dark red]
    max_index = len(colors)-1
    v = float(val-minval) / float(maxval-minval) * max_index
    i1, i2 = int(v), min(int(v)+1, max_index)
    (r1, g1, b1), (r2, g2, b2) = colors[i1], colors[i2]
    f = v - i1
    (r,g,b) = int(r1 + f*(r2-r1)), int(g1 + f*(g2-g1)), int(b1 + f*(b2-b1))
    return "7f{0:02x}{1:02x}{2:02x}".format(r,g,b)   # format each as 2 digit hex with 50% opacity

conn = psycopg2.connect("port=5433 dbname=govhack user=fon")
cur = conn.cursor()
query_old = """
select speed, km_ratio, 
 st_askml(st_setsrid(st_makeline(st_centroid(start.geom), st_centroid(finish.geom)), 4326)) line
from speed, sa2 start, sa2 finish
where speed.start=start.sa2_main11::int and speed.finish=finish.sa2_main11::int
;
"""
query = """
select s.speed, sta.sa2_name11 || ' to ' || fin.sa2_name11 as path, 
  st_askml(st_setsrid(st_makeline(st_centroid(sta.geom), st_centroid(fin.geom)), 4326)) line
from speed s, sa2 sta, sa2 fin
where s.start = sta.sa2_main11::int and s.finish = fin.sa2_main11::int
;
"""
cur.execute(query);

print '<?xml version="1.0" encoding="UTF-8"?>'
print '<kml xmlns="http://www.opengis.net/kml/2.2">'
print '<Document>'

for i,row in enumerate(cur):
    (speed, path, line) = row
    if speed < 28: speed = 28
    if speed > 70: speed = 70
    color = convert_to_rgb(28, 70, speed)
    print """
  <Placemark>
  <Style><LineStyle><color>{color}</color><width>{width}</width></LineStyle></Style>
    <name>{path}</name>
    <description>Speed: {speed} km/h</description>
    {line}
  </Placemark>
    """.format(width=5, color=color, i=i, path=path, speed=speed, line=line)

print '</Document>'
print '</kml>'


"""
<?xml version="1.0" encoding="UTF-8"?>
<kml xmlns="http://www.opengis.net/kml/2.2">
  <Placemark>
    <name>Simple placemark</name>
    <description>Attached to the ground. Intelligently places itself 
       at the height of the underlying terrain.</description>
    <Point>
      <coordinates>-122.0822035425683,37.42228990140251,0</coordinates>
    </Point>
  </Placemark>
</kml>

if __name__ == '__main__':
    minval, maxval = 1, 3
    steps = 10
    delta = float(maxval-minval) / steps
    colors = [(0, 0, 255), (0, 255, 0), (255, 0, 0)]  # [BLUE, GREEN, RED]
    print '  Val       R    B    G'
    for i in xrange(steps+1):
        val = minval + float(i) * delta
        r, g, b = convert_to_rgb(minval, maxval, val, colors)
        print '{:.3f} -> ({:3d}, {:3d}, {:3d})'.format(val, r, g, b)

"""    

#!/usr/bin/env python
import psycopg2
from collections import OrderedDict

USER = "fon"   # customize to match your PG user

conn = psycopg2.connect("dbname=govhack user={}".format(USER))
cur = conn.cursor()

# TODO: generate timeseries in Python with Arrow, and do one SQL query for each hour of data?

cur.execute("select * from stopcount_hourly order by ts asc limit 5000");
heatmap = OrderedDict()

for leg in cur:
    (stopnumber, ts, count, lat, lon) = leg
    heatmap[ts].append(ts,count,lat,lon)

print "var testData = [] ;"

for i, ts in enumerate(heatmap.keys()):
    # print a block of js heatmap format
    print "testData[{}] = {".format(i)
    print "    max: {},".format(heatmap[ts][count])
    # print heatmap[ts]
    pass


"""
var testData = [] ;

testData[0] = {
	max: 5.3133757258,
	data: [
		{lat:-32.14796,lng:116.020217222222,count:2.20411998265592},
		{lat:-32.144985,lng:116.018336666667,count:1.69019608002851},
		{lat:-32.1420722222222,lng:116.017182777778,count:1.77085201164214},
		{lat:-31.9595866666667,lng:115.857668888889,count:4.48627458662737}
	]
};

testData[1] = {
	max: 5.3133757258,
	data: [
		{lat:-32.14796,lng:116.020217222222,count:2.20411998265592},
		{lat:-32.144985,lng:116.018336666667,count:1.69019608002851},
		{lat:-32.1420722222222,lng:116.017182777778,count:1.77085201164214},
		{lat:-31.9595866666667,lng:115.857668888889,count:4.48627458662737}
	]
};

"""    

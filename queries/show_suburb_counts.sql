select sa2_name11, sa3_name11, st_x(st_centroid(sa2.geom)) lng, st_y(st_centroid(sa2.geom)) lat, count(*)
from sa2
inner join station st on (st_contains(sa2.geom, st.geom))
group by sa2_name11, sa3_name11, sa2.geom;

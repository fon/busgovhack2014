copy
(
with 
max_trip as
(
select journey_id, max(leg_id) leg_id, sum(abs(distance)) full_distance
from journey
group by journey_id
)
select a.journey_id, a.leg_id, c.leg_id, a.ondate, c.offdate, b.full_distance,
ston.station_landmark_id start_station_no, st_x(ston.geom) start_long, st_y(ston.geom) start_lat,
stof.station_landmark_id end_station_no, st_x(ston.geom) end_long, st_y(ston.geom) end_lat
from journey a
inner join max_trip b on (b.journey_id = a.journey_id)
inner join journey c on (c.journey_id = b.journey_id and c.leg_id = b.leg_id)
inner join station ston on (ston.station_id = a.onstation_id)
inner join station stof on (stof.station_id = c.offstation_id)
where a.ontype_id = 0
)
to stdout with csv header;

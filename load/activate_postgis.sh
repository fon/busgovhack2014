psql -d govhack2 -f /usr/share/postgresql/9.3/contrib/postgis-2.1/postgis.sql
psql -d govhack2 -f /usr/share/postgresql/9.3/contrib/postgis-2.1/spatial_ref_sys.sql
psql -d govhack2 -f /usr/share/postgresql/9.3/contrib/postgis-2.1/postgis_comments.sql
psql -d govhack2 -c "GRANT SELECT ON spatial_ref_sys TO PUBLIC;"
psql -d govhack2 -c "GRANT ALL ON geometry_columns TO fon;"

drop table mode cascade;
create table mode
(
mode_id integer primary key,
mode_desc varchar(50)
);


drop table tagtype cascade;
create table tagtype
(
type_id integer primary key,
type_desc varchar(50)
);

-- drop table landmark;
-- create table landmark
-- (
-- landmark_id integer,
-- landmark_desc varchar(50)
-- );

drop table station_type cascade;
create table station_type
(
station_type_id integer primary key,
station_type_desc varchar(50)
);

drop table station cascade;
create table station
(
station_id serial primary key,
station_landmark_id integer,
station_type_id integer references station_type,
station_desc varchar(50)
);

drop table journey cascade;
create table journey
(
journey_id integer,
leg_id integer,
card_id integer,
ontype_id integer references tagtype(type_id),
onmode_id integer references mode(mode_id),
onstation_id integer references station(station_id),
offstation_id integer references station(station_id),
ondate timestamp,
offdate timestamp,
distance numeric(10,2)
--primary key (journey_id, leg_id)
);


insert into mode (mode_id, mode_desc) values(0, 'Train');
insert into mode (mode_id, mode_desc) values(1, 'Bus');
insert into mode (mode_id, mode_desc) values(2, 'Ferry');

insert into tagtype (type_id, type_desc) values(0, 'New Journey');
insert into tagtype (type_id, type_desc) values(1, 'Transfer');

insert into station_type (station_type_id, station_type_desc) values(0, 'Bus Stop');
insert into station_type (station_type_id, station_type_desc) values(1, 'Train Station');

insert into station (station_type_id, station_landmark_id, station_desc)
select 0, 
to_number(case when onlandmark = 'NULL' then null else onlandmark end, '99999'),
onlandmark 
from smartdata where onmode = '0'
and onlandmark <> 'NULL'
union
select 0,
to_number(case when offlandmark = 'NULL' then null else offlandmark end, '99999'),
offlandmark
from smartdata where onmode = '0'
and offlandmark <> 'NULL';

insert into station (station_type_id, station_desc)
select 1, 
onstation 
from smartdata where onmode = '1'
and onstation <> 'NULL'
union
select 1,
offstation
from smartdata where onmode = '1'
and offstation <> 'NULL';

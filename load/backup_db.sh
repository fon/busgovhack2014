# Backup the Production Database (Needs the SSH Port forwarded to work)
pg_dump --host localhost --port 5432 --username "euan" --role "euan" --no-password  --format custom --use-set-session-authorization --verbose --file "govhack_db.dmp" --schema "public" "govhack"

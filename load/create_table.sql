--CardId,OnType,OnMode,OnDate,OnStation,OnLandmark,OffDate,OffStation,OffLandmark,Distance

drop table if exists smartdata;
create table smartdata
(
cardid varchar(100),
ontype varchar(100),
onmode varchar(100),
ondate varchar(100),
onstation varchar(100),
onlandmark varchar(100),
offdate varchar(100),
offstation varchar(100),
offlandmark varchar(100),
distance varchar(100)
);

drop table if exists smart;
create table smart
(
cardid int, -- unique number for each user
ontype int, -- 0 = new, 1 = transfer
onmode int, -- 0 = train, 1 = bus, 2 = ferry
ondate timestamp, -- date of journey leg start
onstation varchar(100), -- name of train station where journey started, or null if not a train
onlandmark int, -- unique number of bus/ferry stop where journey started
offdate timestamp, -- date of journey leg end
offstation varchar(100),
offlandmark int, 
distance float -- distance travelled for this journey leg in km. Negative integer values indicate various errors.
);


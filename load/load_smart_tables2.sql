drop sequence journey_id_seq;
create sequence journey_id_seq;

insert into journey
(journey_id, leg_id, card_id, ontype_id, onmode_id, onstation_id, offstation_id, ondate, offdate, distance)
with sdata as
(
select nextval('journey_id_seq') tripid, cardid, ontype, onmode, to_timestamp(ondate, 'YYYY-MM-DD HH24:MI:SS.MS') ondate, onstation, onlandmark, to_timestamp(offdate, 'YYYY-MM-DD HH24:MI:SS.MS') offdate, offstation, offlandmark, distance 
from smartdata
where ontype = '0' 
and offdate <> 'NULL'
-- and cardid = '14'
),
starts as
(
select tripid, cardid, ontype, onmode, 
ondate, coalesce(case when onstation = 'NULL' then null else onstation end, onlandmark) onstation, 
offdate, coalesce(case when offstation = 'NULL' then null else offstation end, offlandmark) offstation, 
distance,
lead(ondate, 1) over (partition by cardid order by ondate) next_ondate
from sdata
)
--select * from starts;
select s.tripid,
row_number() over (partition by a.cardid, tripid order by a.ondate),
to_number(a.cardid, '99999'), to_number(a.ontype, '9'), to_number(a.onmode, '9'), 
-- coalesce(case when a.onstation = 'NULL' then null else a.onstation end, a.onlandmark), 
-- coalesce(case when a.offstation = 'NULL' then null else a.offstation end, a.offlandmark), 
onst.station_id, offst.station_id, 
to_timestamp(a.ondate, 'YYYY-MM-DD HH24:MI:SS.MS') ondate, 
to_timestamp(a.offdate, 'YYYY-MM-DD HH24:MI:SS.MS') offdate, 
to_number(a.distance, '999.99')
from starts s
inner join smartdata a on 
(
a.cardid = s.cardid 
and to_timestamp(a.ondate, 'YYYY-MM-DD HH24:MI:SS.MS') >= s.ondate 
and to_timestamp(a.offdate, 'YYYY-MM-DD HH24:MI:SS.MS') < coalesce(s.next_ondate, to_date('2100-12-31', 'YYYY-MM-DD'))
)
inner join station onst on (onst.station_desc = coalesce(case when a.onstation = 'NULL' then null else a.onstation end, a.onlandmark))
inner join station offst on (offst.station_desc = coalesce(case when a.offstation = 'NULL' then null else a.offstation end, a.offlandmark))
where a.offdate <> 'NULL'
-- order by cardid, a.ondate
;

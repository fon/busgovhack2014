insert into smart (cardid, ontype, onmode, ondate, onstation, onlandmark, 
                   offdate, offstation, offlandmark, distance)
select cast(cardid as int), cast(ontype as int), cast(onmode as int), cast(ondate as timestamp), 
  onstation, cast(onlandmark as int), cast(offdate as timestamp), offstation, cast(offlandmark as int), 
  cast(distance as float)
from smartdata;

create index on smart (cardid);
create index on smart (ontype);
create index on smart (onmode);
create index on smart (ondate);
create index on smart (offdate);

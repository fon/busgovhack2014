#!/bin/bash
for file in *.rpt
do
  tail --lines=+2 $file | head --lines=-3 > ${file}.clean
  echo Loading $file
  psql -d govhack -c "copy smartdata from stdin delimiters ',' CSV;" < ${file}.clean
  rm ${file}.clean
done
